FROM centos:8


RUN mkdir -p /gem_base/targetOS/RTEMS/source
USER root
RUN groupadd gembuild
RUN chgrp -R gembuild /gem_base
RUN chmod -R g+s /gem_base
RUN chmod -R 775 /gem_base
# RUN usermod -a -G gembuild <username>

RUN yum -y install epel-release
RUN yum -y install dnf-plugins-core
RUN yum config-manager --set-enabled PowerTools

RUN yum -y install gcc gcc-c++ glibc-devel make git cmake3 java-1.8.0-openjdk-headless readline-devel

RUN yum -y install iproute net-tools

RUN yum -y install wget make automake autoconf python3 python3-devel bison flex gcc gcc-c++ texinfo patch git readline-devel re2c java; alternatives --set python /usr/bin/python3
# RUN yum -y install make automake autoconf python3 python3-devel bison flex gcc gcc-c++; alternatives --set python /usr/bin/python3
RUN yum -y install unzip
RUN yum -y install bzip2
RUN yum -y install texinfo
RUN yum clean all

# install cross-compiler toolchain
RUN cd /gem_base/targetOS/RTEMS/source && \
 git clone -b 4.10 git://git.rtems.org/rtems-source-builder.git && \
 cd rtems-source-builder/rtems && \
 ../source-builder/sb-set-builder --log=l-powerpc.txt --without-rtems --prefix=/gem_base/targetOS/RTEMS/rtems-4.10 4.10/rtems-powerpc

# install RTEMS
RUN export PATH=/gem_base/targetOS/RTEMS/rtems-4.10/bin:$PATH && \
 cd /gem_base/targetOS/RTEMS/source && \
 mkdir rtems && \
 cd rtems && \
 wget --passive-ftp --no-directories --retr-symlinks https://git.rtems.org/rtems/snapshot/rtems-4.10.2.tar.bz2 && \
 tar xjvf rtems-4.10.2.tar.bz2 && \
 cd /gem_base/targetOS/RTEMS/source/rtems/rtems-4.10.2 && \
 ./bootstrap && \
 mkdir build && \
 cd build && \
 ../configure --target=powerpc-rtems4.10 --prefix=/gem_base/targetOS/RTEMS/rtems-4.10 --enable-cxx --enable-rdbg --disable-tests --enable-networking --enable-posix --enable-rtemsbsp="beatnik mvme2307 mvme3100 qemuppc" && \
 make && \
 make install

# install RTEMS addons
RUN export PATH=/gem_base/targetOS/RTEMS/rtems-4.10/bin:$PATH && \
 cd /gem_base/targetOS/RTEMS/source/rtems && \
 wget https://ftp.rtems.org/pub/rtems/releases/4.10/4.10.2/rtems-addon-packages-4.10.2.tar.bz2 && \
 tar xjvf rtems-addon-packages-4.10.2.tar.bz2 && \
 cd rtems-addon-packages-4.10.2 && \
 export RTEMS_MAKEFILE_PATH=/gem_base/targetOS/RTEMS/rtems-4.10/powerpc-rtems4.10/beatnik && \
 ./bit && \
 export RTEMS_MAKEFILE_PATH=/gem_base/targetOS/RTEMS/rtems-4.10/powerpc-rtems4.10/mvme2307 && \
 ./bit && \
 export RTEMS_MAKEFILE_PATH=/gem_base/targetOS/RTEMS/rtems-4.10/powerpc-rtems4.10/mvme3100 && \
 ./bit && \
 export RTEMS_MAKEFILE_PATH=/gem_base/targetOS/RTEMS/rtems-4.10/powerpc-rtems4.10/qemuppc && \
 ./bit


# install BSP 
RUN export PATH=/gem_base/targetOS/RTEMS/rtems-4.10/bin:$PATH && \
 cd /gem_base/targetOS/RTEMS/source/rtems && \
 wget --passive-ftp --no-directories --retr-symlinks http://www.slac.stanford.edu/~strauman/rtems/ssrlapps-R_libbspExt_1_6.tgz && \
 tar xzvf ssrlapps-R_libbspExt_1_6.tgz && \
 cd /gem_base/targetOS/RTEMS/source/rtems/ssrlapps-R_libbspExt_1_6 && \
 mkdir build && \
 cd build && \
 ../configure --with-rtems-top=/gem_base/targetOS/RTEMS/rtems-4.10 --prefix=/gem_base/targetOS/RTEMS/rtems-4.10 --with-package-subdir=. --enable-std-rtems-installdirs && \
 make && \
 make install


# install tito
RUN yum -y install tito 

RUN yum -y install rsync vim-enhanced
RUN yum clean all

# create gem-rtsw.repo: This is a hack for now, ww should create an rpm for that
RUN echo "[gem-rtsw]" > /etc/yum.repos.d/gem-rtsw.repo && \
 echo "name=Gemini RTSW group software packages" >> /etc/yum.repos.d/gem-rtsw.repo && \
 echo "baseurl=http://hbfswgrepo-lv1.hi.gemini.edu/repo/gembase/" >> /etc/yum.repos.d/gem-rtsw.repo && \
 echo "gpgcheck=0" >> /etc/yum.repos.d/gem-rtsw.repo && \
 echo "timeout=0" >> /etc/yum.repos.d/gem-rtsw.repo



CMD ["/bin/bash"]

