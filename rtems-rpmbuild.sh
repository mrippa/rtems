#!/bin/bash
# rpm build script for gmosdc-dsp module which sadly
# still depends on Solaris compilers.

function usage {                                                                                                                        
                                                                                                                                        
echo "USAGE:                                                                                                                            
`basename $0` rtems [gemtop] [options]                                                                                                                 
This script will:

1. Check that <gemini-package> is fully commited in svn. 
2. Build a gemini rpm package based on <gemini-package>.
3. Sync the local rpm with the REPO

Options:
[-h] Show this message.
"

}

SVN_CHECK=1
# Parse some optional input 
for i in $*; do

       case $1 in
            --nosvncheck) SVN_CHECK=0; shift;;
            -h|--help) usage; exit 1;;
       esac
done

# Start TOP defines
ARCH=`uname -m`
TOP_BUILDDIR=/home/mrippa/work/tmp
RPM_LOCALTMPDIR=.rpmtmp.$$
name=$1
major=4
minor=10
patch=2
version=$major.$minor.$patch
nvr_full=$name-$version
nvr_short=$name-$major.$minor
rtems_top=$nvr
GEMTOP=$2 

function cleanup
{
	echo cleaning up ...
	set -x
	pushd $TOP_BUILDDIR
	rm -rf $RPM_LOCALTMPDIR
	popd
	#rm -rf $INFODIR/$name
	set +x
}

function HLINE
{
	echo ""
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo ""
}


function FATAL  
{
	echo "***"
	echo "*** failed: $*"
	echo "***"
	# mail result [..]
	exit 1
}

# Main starts here:
if [ -z $name ]; then
   echo "Need to give the package name as arg1"
   exit -1;
fi

if [ -z $GEMTOP ]; then
    echo "Need to give the GEMTOP (_topdir) name as arg2"
   exit -1;
fi

rpmtargetdir=$TOP_BUILDDIR/$RPM_LOCALTMPDIR
echo "RPM build directory =  $rpmtargetdir"

echo "Create rpm directory structure ..."
mkdir -p $rpmtargetdir/{BUILD,RPMS,S{OURCE,PEC,RPM}S} || FATAL Failed to make rpm directories

# Main starts here:
if [ -e $name.spec ]; then
   echo "Copied $name.spec to $rpmtargetdir/SPECS/..."
   cp $name.spec $rpmtargetdir/SPECS/
else
   echo "Need to give the package name as ar"
   exit -1;
fi

set -x
echo "Getting RTEMS SB..."
mysourceroot=$rpmtargetdir/SOURCES
mybuilddir=$rpmtargetdir/BUILD/$nvr_short
mybuildrootdir=$rpmtargetdir/BUILDROOT
#mybuildroot=$mybuildrootdir/$nvr_full.$arch
myprefix=$GEMTOP

cd $mysourceroot
gitpath=git://git.rtems.org/rtems-source-builder.git
git clone -b 4.10 $gitpath
cd rtems-source-builder/rtems
../source-builder/sb-set-builder --log=l-powerpc.txt --without-rtems --prefix=$mybuilddir 4.10/rtems-powerpc

echo "Building RTEMS..."
export PATH=$mybuilddir/bin:$PATH

mkdir -p $mysourceroot/$name
cd $mysourceroot/$name

#Get RTEMS and build it
wget --passive-ftp --no-directories --retr-symlinks https://git.rtems.org/rtems/snapshot/rtems-4.10.2.tar.bz2
tar xjvf rtems-4.10.2.tar.bz2

cd $mysourceroot/$name/$nvr_full
./bootstrap
mkdir build
cd build
../configure --target=powerpc-rtems4.10 --prefix=$mybuilddir --enable-cxx --enable-rdbg --disable-tests --enable-networking --enable-posix --enable-rtemsbsp="beatnik mvme2307 mvme3100 qemuppc"


#Patch up rpmmacros for this build. 
echo "write $HOME/.rpmmacros ..."
echo "%_topdir $rpmtargetdir" > $HOME/.rpmmacros
#echo "%arch $ARCH" >> $HOME/.rpmmacrosS
echo "%sourcedir $mysourceroot/$name/$nvr_full" >> $HOME/.rpmmacros
#echo "%rtbranch $RTBRANCH" >> $HOME/.rpmmacros

if [[ $GEMTOP == "" ]]; then
   GEMINI_TOP=/home/mrippa/gem_base
else
   GEMINI_TOP=$GEMTOP
fi

RTEMS_BASE=$GEMINI_TOP/targetOS/RTEMS/$nvr_short

echo "update $rpmtargetdir/SPECS/$name.spec: set _prefix to $GEMINI_TOP"
( ed -s $rpmtargetdir/SPECS/$name.spec << EOF! > /dev/null
g+^%define _prefix.*+s++%define _prefix $RTEMS_BASE+g
.
w
q
EOF!
) || FATAL failed to edit specfile

version=`grep ^"%define version" < $rpmtargetdir/SPECS/$name.spec | awk '{print $3}'`
HLINE
echo "prepare to build version $version..."

#Tar the freshly configured source directory called 'build'
( cd $mysourceroot/$name &&
  echo "create tarball ..." &&
  tar cfz $name-$version.tar.gz $nvr_full &&
  mv $name-$version.tar.gz ..
) || FATAL "failed to prepare for build"

HLINE
echo "Building RPM ..."

#rpmbuild -ba --nodeps $rpmtargetdir/SPECS/$name.spec || FATAL failed to rpmbuild
rpmbuild -ba --noclean --target=$ARCH $rpmtargetdir/SPECS/$name.spec || FATAL failed to rpmbuild
HLINE

echo "Saving RPMs ..."
cp $rpmtargetdir/RPMS/x86_64/rtems-4.10.2-1.el8.x86_64.rpm ~mrippa/RPMS/
cp $rpmtargetdir/SRPMS/rtems-4.10.2-1.el8.src.rpm ~mrippa/SRPMS/

echo "Done!"

