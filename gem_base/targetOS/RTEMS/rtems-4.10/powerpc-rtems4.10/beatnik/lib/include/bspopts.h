/* BSP dependent options file */
/* automatically generated -- DO NOT EDIT!! */

#ifndef __BSP_OPTIONS_H
#define __BSP_OPTIONS_H

/* include/bspopts.tmp.  Generated from bspopts.h.in by configure.  */
/* include/bspopts.h.in.  Generated from configure.ac by autoheader.  */

/* If defined, then the BSP Framework will put a non-zero pattern into the
   RTEMS Workspace and C program heap. This should assist in finding code that
   assumes memory starts set to zero. */
#define BSP_DIRTY_MEMORY 0

/* If defined then the BSP may reduce the available memory size initially.
   This can be useful for debugging (reduce the core size) or dynamic loading
   (std gcc text offsets/jumps are < +/-32M). Note that the policy can still
   be defined by the application (see sbrk.c, BSP_sbrk_policy). By undefining
   CONFIGURE_MALLOC_BSP_SUPPORTS_SBRK this feature is removed and a little
   memory is saved. */
#define CONFIGURE_MALLOC_BSP_SUPPORTS_SBRK 1







/* If defined, then the PowerPC specific code in RTEMS will use data cache
   instructions to optimize the context switch code. This code can conflict
   with debuggers or emulators. It is known to break the Corelis PowerPC
   emulator with at least some combinations of PowerPC 603e revisions and
   emulator versions. The BSP actually contains the call that enables this. */
#define PPC_USE_DATA_CACHE 1

#endif /* __BSP_OPTIONS_H */
