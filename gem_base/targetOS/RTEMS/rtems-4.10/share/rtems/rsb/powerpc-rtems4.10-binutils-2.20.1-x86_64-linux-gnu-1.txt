==============================================================================
RTEMS Tools Project <users@rtems.org> Thu Jul 16 20:05:57 2020
==============================================================================
Report: tools/rtems-binutils-2.20.1-1.cfg
------------------------------------------------------------------------------
RTEMS Source Builder Repository Status
 Remotes:
   1: origin: git://git.rtems.org/rtems-source-builder.git
 Status:
  Clean
 Head:
  Commit: ad29cd659d886c448e0b8ed240f833f4cf437478
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Build Set: (1) tools/rtems-binutils-2.20.1-1.cfg
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Build Set: (2) tools/rtems-binutils-2.20.1-1.cfg
------------------------------------------------------------------------------
Package: powerpc-rtems4.10-binutils-2.20.1-x86_64-linux-gnu-1
 Config: tools/rtems-binutils-2.20.1-1.cfg
 Summary:
  Binutils v2.20.1 for target powerpc-rtems4.10 on host x86_64-linux-gnu
 URL:
  http://sources.redhat.com/binutils
 Version:
  2.20.1
 Release:
  1
  Sources: 1
    1: ftp://ftp.gnu.org/gnu/binutils/binutils-2.20.1.tar.bz2
       md5: 2b9dc8f2b7dbd5ec5992c6e29de0b764
  Patches: 1
    1: https://git.rtems.org/rtems-tools/plain/tools/4.10/binutils/binutils-2.20.1-rtems4.10-20100826.diff
       md5: 733899876e0b32ce0700666b29662d91
    2: https://git.rtems.org/rtems-tools/plain/tools/4.10/binutils/binutils-2.20.1-rtems4.10-20151123.diff
       sha512: ce24ba3e56e7552739c167950a488d80557fdf562dcb527b2e5972c2d18da42a8fd1a47197e54aff0df630d105eb40702f09cad330c193cb8f9309b43b1fb1bc
 Preparation:
  build_top=$(pwd)
  binutils_source="binutils-2.20.1"
  source_dir_binutils=${binutils_source}
  %setup source binutils -q -n ${binutils_source}
  %setup patch binutils -p1
  cd ${build_top}
 Build:
  build_top=$(pwd)
  if test "x86_64-linux-gnu" != "x86_64-linux-gnu" ; then
    # Cross-build (Xc) if no target or the host and target match.
    # Canadian-cross (Cxc) if build, host and target are all different.
    if test -z "powerpc-rtems4.10" -o "x86_64-linux-gnu" == "powerpc-rtems4.10" ; then
      build_dir="build-xc"
    else
      build_dir="build-cxc"
    fi
  else
    build_dir="build"
  fi
  mkdir -p ${build_dir}
  cd ${build_dir}
  # Host and build flags, Cross build if host and build are different and
  # Cxc build if target is deifned and also different.
  # Note, gcc is not ready to be compiled with -std=gnu99 (this needs to be checked).
  if test "x86_64-linux-gnu" != "x86_64-linux-gnu" ; then
    # Cross build
    CC=$(echo "x86_64-linux-gnu-gcc" | sed -e 's,-std=gnu99 ,,')
    CXX=$(echo "x86_64-linux-gnu-g++" | sed -e 's,-std=gnu99 ,,')
    CPPFLAGS="${SB_HOST_CPPFLAGS}"
    CFLAGS="${SB_HOST_CFLAGS}"
    CXXFLAGS="${SB_HOST_CXXFLAGS}"
    LDFLAGS="${SB_HOST_LDFLAGS}"
    # Host
    CPPFLAGS_FOR_HOST="${SB_HOST_CPPFLAGS}"
    CFLAGS_FOR_HOST="${SB_HOST_CFLAGS}"
    CXXFLAGS_FOR_HOST="${SB_HOST_CXXFLAGS}"
    LDFLAGS_FOR_HOST="${SB_HOST_LDFLAGS}"
    CXXFLAGS_FOR_HOST="${SB_HOST_CFLAGS}"
    CC_FOR_HOST=$(echo "gcc ${SB_HOST_CFLAGS}" | sed -e 's,-std=gnu99 ,,')
    CXX_FOR_HOST=$(echo "g++ ${SB_HOST_CXXFLAGS}" | sed -e 's,-std=gnu99 ,,')
    # Build
    CFLAGS_FOR_BUILD="${SB_BUILD_CFLAGS}"
    CXXFLAGS_FOR_BUILD="${SB_BUILD_CXXFLAGS}"
    LDFLAGS_FOR_BUILD="${SB_BUILD_LDFLAGS}"
    CXXFLAGS_FOR_BUILD="${SB_BUILD_CFLAGS}"
    CC_FOR_BUILD=$(echo "/usr/bin/gcc ${SB_BUILD_CFLAGS}" | sed -e 's,-std=gnu99 ,,')
    CXX_FOR_BUILD=$(echo "/usr/bin/g++ ${SB_BUILD_CXXFLAGS}" | sed -e 's,-std=gnu99 ,,')
  else
    LDFLAGS="${SB_BUILD_LDFLAGS}"
    CC=$(echo "/usr/bin/gcc ${SB_BUILD_CFLAGS}" | sed -e 's,-std=gnu99 ,,')
    CXX=$(echo "/usr/bin/g++ ${SB_BUILD_CXXFLAGS}" | sed -e 's,-std=gnu99 ,,')
    CC_FOR_BUILD=${CC}
    CXX_FOR_BUILD=${CXX}
  fi
  export CC CXX CPPFLAGS CFLAGS CXXFLAGS LDFLAGS
  export CC_FOR_HOST CXX_FOR_HOST CPPFLAGS_FOR_HOST CFLAGS_FOR_HOST CXXFLAGS_FOR_HOST LDFLAGS_FOR_HOST
  export CC_FOR_BUILD CXX_FOR_BUILD CFLAGS_FOR_BUILD CXXFLAGS_FOR_BUILD LDFLAGS_FOR_BUILD
  ../${source_dir_binutils}/configure \
  --build=x86_64-linux-gnu --host=x86_64-linux-gnu \
  --target=powerpc-rtems4.10 \
  --verbose --disable-nls \
  \
  --without-included-gettext \
  --disable-win32-registry \
  --disable-werror \
  --prefix=/gem_base/targetOS/RTEMS/rtems-4.10 --bindir=/gem_base/targetOS/RTEMS/rtems-4.10/bin \
  --exec-prefix=/gem_base/targetOS/RTEMS/rtems-4.10 \
  --includedir=/gem_base/targetOS/RTEMS/rtems-4.10/include --libdir=/gem_base/targetOS/RTEMS/rtems-4.10/lib \
  --mandir=/gem_base/targetOS/RTEMS/rtems-4.10/share/man --infodir=/gem_base/targetOS/RTEMS/rtems-4.10/share/info
  make -j 3 all
  cd ${build_top}
 Install:
  cd ${build_top}
  /bin/rm -rf $SB_BUILD_ROOT
  cd ${build_dir}
  make DESTDIR=$SB_BUILD_ROOT install
  /bin/rm -rf $SB_BUILD_ROOT/gem_base/targetOS/RTEMS/rtems-4.10/share/info/configure.info*
  /bin/rm -f $SB_BUILD_ROOT/gem_base/targetOS/RTEMS/rtems-4.10/share/info/dir
  touch $SB_BUILD_ROOT/gem_base/targetOS/RTEMS/rtems-4.10/share/info/dir
  mkdir -p $SB_BUILD_ROOT/gem_base/targetOS/RTEMS/rtems-4.10/share/locale
  /bin/rm -f ${SB_BUILD_ROOT}/gem_base/targetOS/RTEMS/rtems-4.10/lib/libiberty*
  if test ! -f ${SB_BUILD_ROOT}/gem_base/targetOS/RTEMS/rtems-4.10/bin/powerpc-rtems4.10-dlltool; then
  /bin/rm -f ${SB_BUILD_ROOT}/gem_base/targetOS/RTEMS/rtems-4.10/share/man/man1/powerpc-rtems4.10-dlltool*
  fi
  if test ! -f ${SB_BUILD_ROOT}/gem_base/targetOS/RTEMS/rtems-4.10/bin/powerpc-rtems4.10-nlmconv; then
  /bin/rm -f ${SB_BUILD_ROOT}/gem_base/targetOS/RTEMS/rtems-4.10/share/man/man1/powerpc-rtems4.10-nlmconv*
  fi
  if test ! -f ${SB_BUILD_ROOT}/gem_base/targetOS/RTEMS/rtems-4.10/bin/powerpc-rtems4.10-windres; then
  /bin/rm -f ${SB_BUILD_ROOT}/gem_base/targetOS/RTEMS/rtems-4.10/share/man/man1/powerpc-rtems4.10-windres*
  fi
  if test ! -f ${SB_BUILD_ROOT}/gem_base/targetOS/RTEMS/rtems-4.10/bin/powerpc-rtems4.10-windmc; then
  /bin/rm -f ${SB_BUILD_ROOT}/gem_base/targetOS/RTEMS/rtems-4.10/share/man/man1/powerpc-rtems4.10-windmc*
  fi
  cd ${build_top}
